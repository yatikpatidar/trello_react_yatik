import React, { useState } from 'react'
import Form from './commonComponent/Form';


import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@mui/icons-material/Delete'
import Checkbox from '@mui/material/Checkbox';
import Button from '@mui/material/Button';
import Progress from './commonComponent/Progress';


export default function DisplayChecklist({ checklist, Item, onDeleteChecklist, onUpdateCheckItem, onDeleteCheckItem, onAddingCheckItem }) {

    const [showAddCheckItemForm, setShowAddCheckItemForm] = useState(false)

    function handleDeleteChecklist(e, id) {
        onDeleteChecklist(e, id);
    }

    function handleCheckItemCheckbox(e, itemID, checklistId) {
        onUpdateCheckItem(e, itemID, checklistId)
    }

    function handleDeleteCheckItem(e, itemID, checklistId) {
        onDeleteCheckItem(e, itemID, checklistId)
    }

    function handleToggleButton(e) {
        e.stopPropagation();
        setShowAddCheckItemForm(!showAddCheckItemForm)
    }
    function handleAddCheckItem(value) {
        onAddingCheckItem(value, checklist['id'])
    }

    return (

        <Box component="section" spacing={4} sx={{ mb: 2, p: 1, borderRadius: 2, border: '1px solid black', boxShadow: 4 }}>
            <Item sx={{ display: 'flex', justifyContent: 'space-between' }}><Typography gutterBottom variant="h6" component="div" sx={{ mt: 1, ml: 1, textDecoration: 'none' }} >
                {checklist['name']}
            </Typography>
                {checklist['checkItems'].length > 0 && (
                    <Checkbox disabled checked />
                )}
                <Button onClick={(e)=> handleToggleButton(e) } variant="" >Add CheckItem</Button>
                <IconButton onClick={(e) => handleDeleteChecklist(e, checklist['id'])} aria-label="delete" sx={{ '&:hover': { color: "#b71c1c" } }}>
                    <DeleteIcon fontSize="small" />
                </IconButton>

            </Item>
            <Box sx={{ m: 1 }} >

                <Box sx={{ display: 'flex', alignItems: 'center' }}>
                    <Progress checkItems={checklist['checkItems']} />
                </Box>
                {checklist['checkItems'].map((item, index) => {
                    return <Item key={`checkItemsKey${index}`} sx={{ display: 'flex', justifyContent: 'space-between' }}>
                        <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
                            <Checkbox onChange={(e) => handleCheckItemCheckbox(e, item['id'], checklist['id'])} checked={item.state == 'complete'} />
                            <Typography gutterBottom variant="h6" component="div" sx={{ textDecoration: 'none' }} >
                                {item['name']}
                            </Typography>
                        </Box>
                        <IconButton onClick={(e) => handleDeleteCheckItem(e, item['id'], checklist['id'])} aria-label="delete" sx={{ '&:hover': { color: "#b71c1c" } }}>
                            <DeleteIcon fontSize="small" />
                        </IconButton>
                    </Item>
                })}
                {showAddCheckItemForm && (
                    <Form onSubmitForm={handleAddCheckItem} label={"CheckItem"} />
                )}

            </Box>
        </Box>
    )

}
