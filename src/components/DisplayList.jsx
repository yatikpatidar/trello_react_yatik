import React, { useState, useEffect, useContext } from 'react'
import DisplayCard from './DisplayCard';
import Form from './commonComponent/Form';
import ErrorHandling from './commonComponent/ToastError';
import CredentialContext from '../keyAndTokenContext/CredentialContext';
import axios from 'axios';

import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@mui/icons-material/Delete';


const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
}));

export default function DisplayList({ list, onDeleteList, setLoader }) {

    const { user } = useContext(CredentialContext)

    const [cardData, setCardData] = useState([]);
    const [error, setError] = useState('');

    useEffect(() => {

        async function fetchCardData() {

            try {
                setLoader(true)
                let jsonData = await axios.get(`https://api.trello.com/1/lists/${list['id']}/cards?key=${user['key']}&token=${user['token']}`)
                jsonData = jsonData['data']
                setCardData(jsonData);

            } catch (err) {
                setError(`${err.message} reason:- ${err.response.data}`)
            }
            finally {
                setLoader(false)
            }
        }
        fetchCardData()

    }, [])

    function handleDeleteList(id) {
        onDeleteList(id)
    }

    async function handleAddCard(cardname) {
        try {
            setLoader(true)
            let response = await axios.post(`https://api.trello.com/1/cards?idList=${list['id']}&name=${cardname}&key=${user['key']}&token=${user['token']}`)

            setCardData([response.data, ...cardData])

        } catch (err) {
            setError(`${err.message} reason:- ${err.response.data}`)
        }
        finally {
            setLoader(false)
        }
    }

    async function handleDeleteCard(id) {

        try {
            setLoader(true)
            const response = await axios.delete(`https://api.trello.com/1/cards/${id}?key=${user['key']}&token=${user['token']}`)

            setCardData(
                cardData.filter((card) => card['id'] !== id)
            )

        } catch (err) {
            setError(`${err.message} reason:- ${err.response.data}`)
        } finally {
            setLoader(false)
        }
    }


    return (
        <>
            {error ? <ErrorHandling error={error} /> : (
                <Box component="section" spacing={4} sx={{ m: 1, p: 2, minWidth: '25%', border: '2px solid black', borderRadius: 4, boxShadow: 2 }}>
                    <Item sx={{ boxShadow: 3, display: 'flex', justifyContent: 'space-between' }}><Typography gutterBottom variant="h5" component="div" sx={{ textDecoration: 'none', ml: 2 }} >
                        {list['name']}
                    </Typography>
                        <IconButton onClick={() => handleDeleteList(list['id'])} aria-label="delete" sx={{ '&:hover': { color: "#b71c1c" } }}>
                            <DeleteIcon />
                        </IconButton>
                    </Item>

                    <Box component="section" sx={{ display: 'column', p: 2, boxShadow: 3 }}>
                        {cardData.map((card, index) => {
                            return <DisplayCard key={`card${index}`} card={card} Item={Item} onDeleteCard={handleDeleteCard} setLoader={setLoader} />
                        })}
                    </Box>

                    <Form onSubmitForm={handleAddCard} label={"Card"} />

                </Box>
            )}
        </>
    )
}
