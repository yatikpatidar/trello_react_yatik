import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';

export default function Loader({ loader }) {
    return (
        loader ? (
            <Box sx={{
                position:
                    'absolute',
                top: '50%',
                left: '50%',
                transform: 'translate(-50%, -50%)',
                zIndex: 9999,
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                borderRadius: '50%',
                padding: 2,
            }}>
                <CircularProgress />
            </Box>
        ) : (<div></div>)
    );
}
