import React, { useState, useContext } from 'react'
import DisplayChecklist from './DisplayChecklist';
import Form from './commonComponent/Form';
import ErrorHandling from './commonComponent/ToastError';
import CredentialContext from '../keyAndTokenContext/CredentialContext';
import axios from 'axios';

import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';
import Stack from '@mui/material/Stack';
import DeleteIcon from '@mui/icons-material/Delete'
import Button from '@mui/material/Button';
import CloseIcon from '@mui/icons-material/Close';
import Modal from '@mui/material/Modal';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    borderRadius: 2,
    p: 2,
    height: '80vh'
};

export default function DisplayCard({ card, Item, onDeleteCard, setLoader }) {

    const { user } = useContext(CredentialContext)

    const [checklistData, setChecklistData] = useState([])
    const [open, setOpen] = useState(false);
    const [error, setError] = useState('')
    const [showAddChecklistForm, setShowAddCheckListForm] = useState(false)

    function handleClose(e) {
        e.stopPropagation();
        setShowAddCheckListForm(false)
        setOpen(false)
    };

    function handleDelete(e, id) {
        e.stopPropagation();
        onDeleteCard(id)
    }

    function handleToggleButton(e) {
        e.stopPropagation();
        setShowAddCheckListForm(!showAddChecklistForm)
    }

    function handleClickOpen(e, id) {
        
        setOpen(true)
        async function fetchCheckList() {
            try {
                setLoader(true)

                let jsonData = await axios.get(`https://api.trello.com/1/cards/${id}/checklists?key=${user['key']}&token=${user['token']}`)
                jsonData = jsonData['data']
                setChecklistData(jsonData);

            } catch (err) {
                setError(`${err.message} reason:- ${err.response.data}`)
            } finally {
                setLoader(false)
            }
        }
        fetchCheckList()
    }

    async function handleAddChecklist(value) {
        try {
            setLoader(true)
            const checklistName = value

            let response = await axios.post(`https://api.trello.com/1/cards/${card['id']}/checklists?name=${checklistName}&key=${user['key']}&token=${user['token']}`)

            setChecklistData([...checklistData, response.data])

        } catch (err) {
            setError(`${err.message} reason:- ${err.response.data}`)
        }
        finally {
            setLoader(false)
        }
    }

    async function handleDeleteChecklist(e, idChecklist) {

        e.stopPropagation();
        try {
            setLoader(true)

            const response = await axios.delete(`https://api.trello.com/1/cards/${card['id']}/checklists/${idChecklist}?key=${user['key']}&token=${user['token']}`)

            setChecklistData(
                checklistData.filter((checklist) => checklist['id'] !== idChecklist)
            )

        } catch (err) {
            setError(`${err.message} reason:- ${err.response.data}`)
        } finally {
            setLoader(false)
        }
    }


    async function handleUpdateCheckItem(e, checkItemId, checklistId) {

        e.stopPropagation()
        try {
            setLoader(true)
            const state = e.target.checked ? 'complete' : 'incomplete'
            const res = await axios.put(`https://api.trello.com/1/cards/${card['id']}/checkItem/${checkItemId}?key=${user['key']}&token=${user['token']}&state=${state}`)

            const updatedChecklist = checklistData.map((checklist) => {
                if (checklist['id'] === checklistId) {
                    return {
                        ...checklist, checkItems:

                            checklist['checkItems'].map((checkItem) => {
                                if (checkItem['id'] === checkItemId) {
                                    return res.data
                                } else {
                                    return checkItem
                                }
                            })
                    }
                }
                else {
                    return checklist
                }
            })
            setChecklistData(updatedChecklist)

        } catch (err) {
            setError(`${err.message} reason:- ${err.response.data}`)
        } finally {
            setLoader(false)
        }
    }

    async function handleDeleteCheckItem(e, checkItemId, checklistId) {

        e.stopPropagation();
        try {
            setLoader(true)
            const response = await axios.delete(`https://api.trello.com/1/checklists/${checklistId}/checkItems/${checkItemId}?key=${user['key']}&token=${user['token']}`)

            const updatedChecklist = checklistData.map((checklist) => {
                if (checklist['id'] === checklistId) {
                    return {
                        ...checklist, checkItems:

                            checklist['checkItems'].filter((checkItem) => {
                                if (checkItem['id'] !== checkItemId) {
                                    return true
                                }
                                return false
                            })
                    }
                }
                else {
                    return checklist
                }
            })
            setChecklistData(updatedChecklist)

        } catch (err) {
            setError(`${err.message} reason:- ${err.response.data}`)
        } finally {
            setLoader(false)
        }
    }

    async function handleNewCheckItem(value, checklistId) {
        
        try {
            setLoader(true)
            const response = await axios.post(`https://api.trello.com/1/checklists/${checklistId}/checkItems?name=${value}&key=${user['key']}&token=${user['token']}`)

            const updatedChecklist = checklistData.map((checklist) => {
                if (checklist['id'] === checklistId) {
                    return {
                        ...checklist, checkItems: [...checklist['checkItems'], response.data]
                    }
                }
                else {
                    return checklist
                }
            })

            setChecklistData(updatedChecklist)
        } catch (err) {
            setError(`${err.message} reason:- ${err.response.data}`)
        } finally {
            setLoader(false)
        }
    }

    return (

        error !== "" ? (< ErrorHandling error={error} />) : (

           <>
            <Box onClick={(e) => handleClickOpen(e, card['id'])} component="section" spacing={4} sx={{ p: 1, borderRadius: 2 }}>
                <Item sx={{ display: 'flex', justifyContent: 'space-between' }}><Typography gutterBottom variant="h6" component="div" sx={{ textDecoration: 'none' }} >
                    {card['name']}
                </Typography>
                    <IconButton onClick={(e) => handleDelete(e, card['id'])} aria-label="delete" sx={{ '&:hover': { color: "#b71c1c" } }}>
                        <DeleteIcon fontSize="small" />
                    </IconButton>
                </Item>
                </Box>
                <Modal
                    open={open}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"

                >
                    <Box sx={style} >
                        <Box sx={{ display: 'flex', justifyContent: 'space-between', ml: 3 }}>
                            <Typography id="modal-modal-title" variant="h4" component="h2">
                                {card['name']}
                            </Typography>

                            <Box sx={{ display: 'flex', justifyContent: 'space-between' }} >
                                <Button onClick={(e) => handleToggleButton(e)} variant="" >Add Checklist</Button>
                                <IconButton onClick={(e) => handleClose(e)} sx={{ color: 'black', '&:hover': { color: "#b71c1c" } }}>
                                    <CloseIcon />
                                </IconButton>
                            </Box>
                        </Box>

                        <Stack spacing={2} sx={{ mt: 2 }} direction="row">
                            <div style={{ margin: 3, display: 'flex', flexDirection: 'column', width: '100%', maxHeight: '70vh', overflowY: 'auto' }}>

                                {checklistData.map((checklist, index) => {
                                    return <div key={`checklist${index}`}>
                                        <DisplayChecklist
                                            checklist={checklist}
                                            Item={Item}
                                            onDeleteChecklist={handleDeleteChecklist}
                                            onUpdateCheckItem={handleUpdateCheckItem}
                                            onDeleteCheckItem={handleDeleteCheckItem}
                                            onAddingCheckItem={handleNewCheckItem}

                                        />
                                    </div>
                                })}

                                {showAddChecklistForm && (
                                    <Form onSubmitForm={handleAddChecklist} label={"Checklist"} />
                                )}
                            </div>

                        </Stack>
                    </Box>
                </Modal></>
            
        )
    )
}

