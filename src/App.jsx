import React from 'react'
import { Route, RouterProvider, createRoutesFromElements, createBrowserRouter } from 'react-router-dom'
import Layout from './Layout'
import Home from './pages/Home'
import AllList from './pages/AllList'
import NotFound from './components/commonComponent/NotFound'

export default function App() {

    const router = createBrowserRouter(
        createRoutesFromElements(
            <Route path='/' element={<Layout />}>
                <Route path='' element={<Home />} />
                <Route path='boards/:id' element={<AllList />} />
                <Route path='*' element={<NotFound />} />
            </Route>
        )
    )
    return (
        <RouterProvider router={router} />
    )
}

