import React, { useEffect, useState, useContext } from 'react'
import { useParams, Link } from 'react-router-dom'
import axios from 'axios'
import DisplayList from '../components/DisplayList';
import CreateList from '../components/commonComponent/AddModal';
import ErrorHandling from '../components/commonComponent/ToastError';
import CredentialContext from '../keyAndTokenContext/CredentialContext';
import Loader from '../components/commonComponent/Loader';

import Divider from '@mui/material/Divider';
import Stack from '@mui/material/Stack';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';


export default function AllList() {

    const { id } = useParams()
    const { user } = useContext(CredentialContext)

    const [listData, setListData] = useState([])
    const [error, setError] = useState('')
    const [loader, setLoader] = useState(false)

    useEffect(() => {
        async function fetchListsData() {

            try {
                setLoader(true)
                let jsonData = await axios.get(`https://api.trello.com/1/boards/${id}/lists?key=${user['key']}&token=${user['token']}`);

                jsonData = jsonData['data']
                setListData(jsonData);

            } catch (err) {
                setError(`${err.message} reason:- ${err.response.data}`)
            } finally {
                setLoader(false)
            }
        }
        fetchListsData()
    }, [])

    async function handleNewList(text) {
        console.log(text)
        try {
            setLoader(true)
            const listTitle = text;
            const response = await axios.post(`https://api.trello.com/1/lists?name=${listTitle}&idBoard=${id}&key=${user['key']}&token=${user['token']}`)

            setListData([...listData, response.data])

        } catch (err) {
            setError(`${err.message} reason:- ${err.response.data}`)
        }
        finally {
            setLoader(false)
        }
    }

    async function handleDeleteList(id) {

        try {
            setLoader(true)
            const response = await axios.put(`https://api.trello.com/1/lists/${id}/closed?value=true&key=${user['key']}&token=${user['token']}`)

            setListData(
                listData.filter((list) => list['id'] !== id)
            )

        } catch (err) {
            setError(`${err.message} reason:- ${err.response.data}`)
        }
        finally {
            setLoader(false)
        }
    }

    return (

        error != "" ? (<ErrorHandling error={error} />) :
            (<><div >
                <Stack
                    direction="column"
                    divider={<Divider orientation="vertical" flexItem />}
                    spacing={2}
                    flexShrink={0}
                    flexWrap={'nowrap'}
                    position={'relative'}

                >
                    <Box component="section" sx={{ p: 2, boxShadow: 3 }}>

                        <CreateList onSubmitModal={handleNewList} label={"List"} user={user} />

                    </Box>

                    <Box>
                        <Link to={`/`} style={{ textDecoration: 'none' }} >
                            <Button variant="contained" startIcon={<ArrowBackIcon />} sx={{ ml: 3 }} >Back</Button>
                        </Link>

                    </Box>

                    <Box component="section" sx={{ display: 'flex', width: "100%", p: 2, maxWidth: '95vw', overflowX: 'auto', justifyContent: 'flex-start', alignItems: 'flex-start' }}>
                        {listData.map((list, index) => {
                            return <DisplayList key={`list${index}`} list={list} onDeleteList={handleDeleteList} setLoader={setLoader} />
                        })}

                    </Box>

                    {<Loader loader={loader} />}
                </Stack>

            </div>
            </>
            )

    )
}

