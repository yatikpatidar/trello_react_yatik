import React, { useState, useEffect, useContext } from 'react'
import axios from 'axios'
import DisplayBoard from '../components/DisplayBoard';
import CreateBoard from '../components/commonComponent/AddModal';
import ErrorHandling from '../components/commonComponent/ToastError';
import CredentialContext from '../keyAndTokenContext/CredentialContext';

import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Loader from '../components/commonComponent/Loader';

export default function Home() {
    const { user } = useContext(CredentialContext)

    const [boardsData, setBoardsData] = useState([]);
    const [loader, setLoader] = useState(false)
    const [error, setError] = useState('')

    useEffect(() => {

        async function fetchBoardsData() {

            try {
                setLoader(true)
                let jsonData = await axios.get(`https://api.trello.com/1/members/me/boards?fields=name,url&key=${user['key']}&token=${user['token']}`);
                jsonData = jsonData['data']

                setBoardsData(jsonData);

            } catch (err) {
                setError(`${err.message} reason:- ${err.response.data}`)
            } finally {
                setLoader(false)
            }
        }
        fetchBoardsData()


    }, [])

    async function handleAddBoard(value) {

        try {
            setLoader(true)
            const boardTitle = value;
            const response = await axios.post(`https://api.trello.com/1/boards/?name=${boardTitle}&key=${user['key']}&token=${user['token']}`)

            setBoardsData([response.data, ...boardsData])

        } catch (err) {
            setError(`${err.message} reason:- ${err.response.data}`)
        }
        finally {
            setLoader(false)

        }

    }

    return (
        <>
            {loader ? <Loader loader={loader} /> : (

                error !== "" ? <ErrorHandling error={error} /> :

                    (<>
                        <Box sx={boardAndcreateStyle}>
                            <Typography variant="h5" sx={{ p: 1, px: 4 }} > Boards </Typography>

                            <CreateBoard onSubmitModal={handleAddBoard} label={"Board"} />
                        </Box>

                        <Grid px={5} my={4} container spacing={5} >
                            {(boardsData).map((board, index) => {
                                return (
                                    <Grid key={`boards${index}`} item lg={4} sx={{ textAlign: 'center' }} >
                                        <DisplayBoard board={board} />
                                    </Grid>
                                )
                            })}
                        </Grid>

                    </>)
            )}
        </>
    )
}



const boardAndcreateStyle = {
    display: 'flex',
    justifyContent: 'space-between',
    shadows: '20px',
    width: '60%',
    marginLeft: '20%',
    p: 2,
    borderRadius: 2,
    borderBottom: '2px solid gray',

}